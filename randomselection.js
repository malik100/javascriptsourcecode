var student1 = {
    name:"John",
    studentID: 99999
}   
var student2 = {
    name:"Mary",
    studentID: 12345
}
var student3 = {
    name:"Kevin",
    studentID: 55555
}
let students = [student1, student2, student3];

let rand = Math.floor((Math.random() * students.length));
console.log(rand);
let studentPicked = students[rand];
console.log("The student picked was " + studentPicked.name + ", " + studentPicked.studentID);