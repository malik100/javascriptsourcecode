const stdList = document.getElementById("student-list");

const stdInput = document.getElementById("std-input");

const stdRandom = document.getElementById("random-std-name");

const stdArray = [];
function deleteStudent(event) {
	const clickedElement = event.target.parentElement;
	const removeElement = clickedElement.children[0].textContent;
	// stdArray.forEach(function (x, y, z) {
	// 	if (clickedElement.textContent.includes(x)) {
	// 		z.splice(y, 1);
	// 	}
	// });
	const textIndex = stdArray.indexOf(removeElement);
	stdArray.splice(textIndex, 1);
	clickedElement.remove();
}
function addStudent() {
	if (stdInput.value != "") {
		// stdList.innerHTML += `<li> ${stdInput.value} </li>`;

		// Create new element(li tag)
		const newLiTag = document.createElement("li"); // <li> </li>
		const spanT = document.createElement("span");
		// Set text value of tag
		spanT.textContent = stdInput.value;
		newLiTag.append(spanT); // <li> John </li>

		const deleteButton = document.createElement("button");
		deleteButton.textContent = "Delete";
		deleteButton.style.margin = "10px 20px";
		deleteButton.addEventListener("click", deleteStudent);
		newLiTag.append(deleteButton);

		// Add the li tag to the parent tag
		stdList.appendChild(newLiTag);

		stdArray.push(stdInput.value);
		stdInput.value = "";
	}

	stdInput.focus();
}

function generateRandom() {
	if (stdArray.length === 0) {
		stdRandom.textContent = "No students entered";
	} else {
		const generateRandomNum = Math.floor(Math.random() * stdArray.length);
		const randomStudentName = stdArray[generateRandomNum];

		stdRandom.innerHTML = "<b>" + randomStudentName + "</b>";
		//stdRandom.innerHTML = `<b>  ${randomStudentName} </b>`;
		console.log(stdArray); //just to have a visual
	}
}

function resetList() {
	stdList.textContent = "";
	stdRandom.textContent = "";
	while (stdArray.length != 0) {
		stdArray.pop();
	}
	console.log(stdArray); //just to have a visual
}
