const inputBox = document.getElementById("inputBox");
const starNumber = inputBox.value;
const address = ("https://swapi.dev/api/people/" + starNumber + "/");
const displayArea = document.getElementById("displayArea");
// function getData(){
//     // console.log(starNumber)
//     const oReq = new XMLHttpRequest();
//     oReq.addEventListener('load', reqListener);
//     // const address = `https://swapi.dev/api/people/${starNumber}`
//     oReq.open('GET', "https://swapi.dev/api/people/1");
//     oReq.send();
// }

function getData(){
    const starNumber = inputBox.value;
    console.log(starNumber)
    $.ajax(`https://swapi.dev/api/people/${starNumber}/`).done(reqListener);
    
}

function reqListener(data){
    console.log(data)
    console.log(this);
    const name = data.name;
    displayArea.textContent += " / " + name;
}

// function reqListener(){
//     console.log(this.responseText);
//     console.log(this);
// }
//https://codepen.io/aakashmalhotra/pen/dypWvrG?editors=1011
//https://swapi.dev/documentation#people