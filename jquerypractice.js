console.log('aaa')
// $('h1').hide();              -----tag
// $('#heading1').hide();       -----id
// $('.heading2').hide();       -----class
// $(':button').hide();         -----by type
$('[href').css('color', 'purple');          //attribute
$('a[href="abc"]').css('color', 'green');   //value
// $('*').hide();                       -----select all
$('p span').css('color', 'red');
$('ul#list li:first').css('color', 'red')