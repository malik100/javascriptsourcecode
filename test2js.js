/*fetching main variables from DOM*/
const displayArea = document.getElementById("displayArea");
const foodEntered = document.getElementById("foodName");
const caloriesEntered = document.getElementById("foodCalories");
const foodList = [];

function addItem(){
  /*blan food item to add values to*/
    const foodItem = {
        name:"",
        calories:0,
    }
    /*assigning properties to food*/
    foodItem.name = foodEntered.value;
    foodItem.calories = caloriesEntered.value;
  /*adding food to array*/
    foodList.push(foodItem);
  /*custom sort function that includes casting calories back to number to sort correctly*/
    foodList.sort(function(a, b){
        if(Number(a.calories) < Number(b.calories)){
            return -1;
        }else if(Number(a.calories) > Number(b.calories)){
            return 1;
        }else{
            return 0;
        }
    });
  /*resetting display area to display new sorted list*/
     displayArea.textContent = "";
  /*appending all food items to display area*/
    foodList.forEach(x =>{
        listElement = document.createElement("li");
        listElement.textContent = `${x.name} - ${x.calories}`;
        displayArea.appendChild(listElement);
    })
  /*resetting input bar values*/
    foodEntered.value = ""; 
    caloriesEntered.value = "";
}

let scroll = 0;

// document.addEventListener("scroll", function(event){
//   document.body.style.backgroundColor = "red";
//   console.log('user scrolled')
// })

// document.addEventListener("click", () =>{
//   alert('user clicked');
// })
window.addEventListener('scroll', ()=>{
  const scrollable = document.documentElement.scrollHeight - window.innerHeight;
  const scrolled = window.scrollY;
  if (Math.ceil(scrolled) === scrollable){
    alert('youve reached the bottom');
  }
})

function deleteStudent(event) {
	const clickedElement = event.target.parentElement;
	stdArray.forEach()
	clickedElement.remove();
}

