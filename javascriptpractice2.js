{
    let position = {
        x:10,
        y:20,
        print: function(){
            console.log(`x: ${this.x}, y: ${this.y}`)
        },
        nestedObject: {name:"Nested Object Number One", age:100}
    }
    let myPosition = position;
    console.log(position);
    console.log(myPosition);
    position.print();
    console.log(position.nestedObject.name);
}

function print (){
    console.log("the function print is being called");
}

print();

var name = prompt("please enter a name");

//TERNARY OPERATOR
var question = name === "abc" ? 10 : 0;

