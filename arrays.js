var arr = [4,5,6,7,8,9,10,11];
//  arr.push(10);  //returns <<NEW LENGTH>> of array
//  arr.push(10, 11, 12); <<MULTIPLE ELEMENTS ADDED>>
//  arr.pop();    //returns <<DELETED ELEMENT>>
//  console.log(arr.indexOf(0));
//  console.log(arr.indexOf(6,2)) //the started index is <<INCLUSIVE>>
//  console.log(arr.indexOf(9, -3)); //searches with <<OFFSET FROM MAXIMUM LENGTH>>
var nested = [[1,2,3,4,5], ['a','b','c','d','e','f']];

arr.forEach(e => console.log(e * 100));
//arr.forEach(function(s){console.log(s *10)});
//arr.forEach(FUNCTION, INDEX);

console.log("------------------")
const bar = [4, 9, 2, 5, 8, 4];
var mapped = bar.map((x) => x*x);
console.log(mapped);

// const items = ['item1', 'item2', 'item3']
// console.table(items);
// arr.join(); // joins all elements into a string

/*arr.forEach(function(x, y, z){
    console.log(`${x} is at index ${y} \n\n ${z.push(x)}`)
});*/
console.log(arr)

