
//initilaizing variables and fetching DOM elements
let weather;
const weatherDisplay = document.getElementById('weather');
const dateDisplay = document.getElementById('date');
const timeDisplay = document.getElementById('time');
const email = document.getElementById('email');
const password = document.getElementById('password');
const errorMessage = document.getElementById('errorMessage');
const dayNames = ["Mon","Tue","Wed","Thu","Fri","Sat","Sun",];
const monthNames = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
let cityCode = "";

//Getting the city key to use in the url
function getCity(){
    $.ajax("http://dataservice.accuweather.com/locations/v1/cities/search?apikey=oY0IPcvrPcMcvnchiTmg9QkcStwGEfLD&q=montreal&language=en-us").done(function(data){
        console.log(data);
        cityCode = data[0].Key;
       })
}
getCity();

//function validates email and displays errors on the page if there are any
function validation(){
    errorMessage.textContent = "";
    if(email.value != 'admin@yopmail.com' || password.value.length < 6){
        errorMessage.innerHTML += `Error please complete the form`
    }
    if(email.value != "admin@yopmail.com"){
        errorMessage.innerHTML += `<br>*Email address must be filled in!`
    }
    if(password.value.length < 6){
        errorMessage.innerHTML += `<br> Password length must be at least 6 characters`;
    }
    if(password.value != 'adminyopmail'){
        errorMessage.innerHTML += `<br> Password is incorrct!`;
    }
    if(email.value === 'admin@yopmail.com' && password.value === 'adminyopmail'){
        displayWeather();
    }
}


function seeDateTime(){
    //creating a date object and getting basic time units
    let date = new Date();
    let hours = date.getHours();
    let newHours = 0;
    let AMPM = "";
    //converting clock to 12 hours and changing AM PM
    if(hours > 12 && hours <= 23){
        newHours = hours - 12;
        AMPM = "PM";
    }else{
        newHours = hours + 12;
        AMPM = "AM";
    }
    //appending date and time onto HTML using date object
    dateDisplay.textContent = `Today's Date: ${dayNames[date.getDay()]} ${monthNames[date.getMonth()]} ${date.getMonth() + 1} ${date.getFullYear()}`;
    timeDisplay.textContent = `Time Now: ${newHours}:${date.getMinutes()}:${date.getSeconds()} ${AMPM}`;
    //recursion here every 1 second to update the time by 1 second
    setTimeout("seeDateTime()",1000);
}
seeDateTime();

//recieving API here using the citykey and using a loop to dynamically display data from recieved object 5 times
function displayWeather(){
    $.ajax(`http://dataservice.accuweather.com/forecasts/v1/daily/5day/${cityCode}?apikey=oY0IPcvrPcMcvnchiTmg9QkcStwGEfLD&metric=true`).done(function(data){
        console.log(data);
        weather = data.DailyForecasts;
        //looping 5 times to get info for all 5 days and appending to HTML
        for(let i = 0; i < 5; i++){
            let item = document.createElement("li");
            item.innerHTML = `${weather[i].Date}<br> Max: ${weather[i].Temperature.Maximum.Value}${weather[i].Temperature.Maximum.Unit}
                               Min: ${weather[i].Temperature.Minimum.Value}${weather[i].Temperature.Minimum.Unit}<br>
                               Day: ${weather[i].Day.IconPhrase} Night: ${weather[i].Night.IconPhrase}<br><br>`
            weatherDisplay.appendChild(item);                   
        }
       })
}


